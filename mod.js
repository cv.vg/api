import * as Sentry from 'npm:@sentry/node@7.28.1';
import pogo from 'https://deno.land/x/pogo@v0.5.2/main.ts';
import { stringSpliceMulti } from 'https://git.kaki87.net/KaKi87/strsplice/raw/commit/5e45f90aab225f2d0023716afd1f91ae133594f9/mod.js';
import { hashSync as hash, compareSync as verify } from 'https://deno.land/x/bcrypt@v0.3.0/mod.ts';
import { Cryptr } from 'https://raw.githubusercontent.com/DjDeveloperr/deno_cryptr/9a3a2403ad906a8e19fff5a34ad95f815b67f6dc/mod.ts';
import axios from 'npm:axios';
import Joi from 'npm:joi@17.6.0';
import getSchemas from 'npm:@cv.vg/schemas@0.1.0-dev.8';
import { createServerHelper } from 'https://git.kaki87.net/KaKi87/pogo-helpers/raw/commit/c09bb256d310dad935c9406c322a896b7898fa66/mod.js';

import db from './lib/db.js';
import nanoid from './lib/nanoid.js';
import verifyConditions from './lib/conditions.js';
import settings from './lib/settings.js';

import config from './config.js';
const {
    sentryDsn,
    port,
    webUrl,
    webhookTimeout
} = config;

Sentry.init({
    dsn: sentryDsn
});

const
    server = pogo.server({ port }),
    {
        createSchema,
        deleteSchema,
        getVanitySchema
    } = getSchemas({ Joi });

const
    {
        createRoute
    } = createServerHelper(
        server,
        {
            isCors: true,
            onError: error => console.error(error)
        }
    ),
    {
        getSetting,
        setSetting
    } = settings(db);

const getVanity = async ({ code, id }) => {
    const [
        { rows: [[codeExists] = []] },
        { rows: [[urlExists, urlEditionCode] = []] }
    ] = await Promise.all([
        db.queryArray('SELECT 1 FROM "vanityCode" WHERE "code" = $1', [code]),
        db.queryArray('SELECT 1, "editionCode" FROM "url" WHERE "id" = $1', [id])
    ]);
    return {
        codeExists,
        urlExists,
        urlEditionCode
    };
};

createRoute({
    method: 'GET',
    path: '/',
    handler: async (
        request,
        h,
        {
            params,
            response
        }) => {
        if(typeof params.list === 'string'){
            response.body = (await db.queryArray(
                'SELECT "id" FROM "url"'
            )).rows.map(([
                id
            ]) => id);
        }
        else if(typeof params.count === 'string')
            response.body = Number((await db.queryArray('SELECT COUNT("id") FROM "url"')).rows[0][0]);
        else if(webUrl)
            response.redirect(webUrl);
        else
            response.code(404);
        return response;
    }
});

{
    let idSize;
    const
        schema = createSchema,
        handler = async (
            request,
            h,
            {
                username,
                password,
                path,
                params,
                body,
                response
            }
        ) => {
            let id, urlExists;
            if(username){
                let codeExists, urlEditionCode;
                ({
                    codeExists,
                    urlExists,
                    urlEditionCode
                } = await getVanity({
                    code: username,
                    id: path.id
                }));
                if(codeExists){
                    if(urlExists){
                        response.code(409);
                        return response;
                    }
                    else {
                        await db.queryArray('DELETE FROM "vanityCode" WHERE "code" = $1', [username]);
                        id = path.id;
                    }
                }
                else {
                    if(urlExists){
                        if(verify(username, urlEditionCode))
                            id = path.id;
                        else {
                            response.code(401);
                            return response;
                        }
                    }
                    else {
                        response.code(401);
                        return response;
                    }
                }
            }
            else {
                if(!idSize)
                    idSize = await getSetting('idSize');
                id = nanoid({ size: idSize });
                if((await db.queryArray('SELECT 1 FROM "url" WHERE "id" = $1', [id])).rows[0]?.[0] === 1){
                    idSize += 1;
                    id = nanoid({ size: idSize });
                    await setSetting({ idSize });
                }
            }
            const
                timestamp = Date.now(),
                deletionCode = params.isDeletable && !urlExists ? nanoid({ size: 30 }) : undefined;
            await db.queryArray(`
                INSERT INTO "url" (
                    "id",
                    "creationTimestamp",
                    "${password ? 'encryptedConfig' : 'config'}"
                    ${path.id ? ', "editionCode"' : ''}
                    ${deletionCode ? ', "deletionCode"' : ''}
                    ${body.webhook ? ', "webhookSecretConfig"' : ''}
                )
                VALUES (
                    $id,
                    $timestamp,
                    $${password ? 'encryptedConfig' : 'config'}
                    ${path.id ? ', $editionCode' : ''}
                    ${deletionCode ? ', $deletionCode' : ''}
                    ${body.webhook ? ', $webhookSecretConfig' : ''}
                )
                ON CONFLICT ("id") DO UPDATE SET
                    "editionTimestamp" = $timestamp,
                    "config" = ${password ? 'null' : '$config'},
                    "encryptedConfig" = ${password ? '$encryptedConfig' : 'null'}
                    ${body.webhook ? ', "webhookSecretConfig" = $webhookSecretConfig' : ''}
            `, {
                id,
                timestamp,
                ...password
                    ? { encryptedConfig: await new Cryptr(password).encrypt(JSON.stringify(body)) }
                    : { config: body },
                ...path.id ? { editionCode: hash(username) } : {},
                ...deletionCode ? { deletionCode: hash(deletionCode) } : {},
                ...body.webhook ? {
                    webhookSecretConfig: {
                        webhookSecretType: params.webhookSecretType,
                        webhookSecretKey: params.webhookSecretKey,
                        webhookSecretValue: params.webhookSecretValue
                    }
                } : {}
            });
            response.body = {
                id,
                [urlExists ? 'editionTimestamp' : 'creationTimestamp']: timestamp,
                deletionCode
            };
            return response;
        };
    createRoute({
        method: 'POST',
        path: '/',
        schema,
        handler
    });
    createRoute({
        method: 'PUT',
        path: '/{id}',
        schema,
        handler
    });
}

{
    const handler = async (
        request,
        h,
        {
            timestamp,
            password,
            response,
            path,
            params
        }
    ) => {
        let config;
        const {
            rows: [[
                creationTimestamp,
                deletionTimestamp,
                deletionMessage,
                unencryptedConfig,
                encryptedConfig,
                webhookSecretConfig
            ] = []]
        } = await db.queryArray(`
            SELECT
                "creationTimestamp",
                "deletionTimestamp",
                "deletionMessage",
                "config",
                "encryptedConfig"
                ${request.path.endsWith('/t') ? ', "webhookSecretConfig"' : ''}
            FROM "url"
            WHERE id = $1
        `, [path.id]);
        if(!creationTimestamp){
            response.code(404);
            return response;
        }
        if(deletionTimestamp){
            response.code(410);
            if(deletionMessage){
                const deletionDate = new Date(Number(deletionTimestamp));
                response.body = `
                    <!DOCTYPE html>
                    <html>
                        <head>
                            <meta charset="UTF-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@picocss/pico@latest/css/pico.min.css">
                        </head>
                        <body style="padding: 1rem">
                            <p>
                                This link has been deleted by its author on
                                <time datetime="${deletionDate.toISOString()}">${deletionDate.toLocaleString('en')}</time>,
                                leaving the following message:
                            </p>
                            <blockquote><script>
                                document.write((() => {
                                    const sanitizer = document.createElement('p');
                                    sanitizer.innerHTML = '${deletionMessage}';
                                    return sanitizer.textContent;
                                })());
                            </script></blockquote>
                        </body>
                    </html>
                `;
            }
            return response;
        }
        if(encryptedConfig){
            if(password){
                try {
                    config = JSON.parse(await new Cryptr(password).decrypt(encryptedConfig))
                }
                catch {}
            }
            if(!config){
                response.code(401);
                response.header('www-authenticate', 'Basic');
                return response;
            }
        }
        else
            config = unencryptedConfig;
        let url, randomConfigIndex, conditionalConfigIndex;
        const
            {
                conditions,
                configs,
                isRandom,
                webhook
            } = config,
            unmetCondition = conditions && verifyConditions({ request, timestamp, conditions });
        if(unmetCondition){
            response.body = {
                unmetCondition
            };
            response.code(400);
            return response;
        }
        if(configs){
            if(isRandom)
                randomConfigIndex = Math.floor(Math.random() * configs.length);
            else
                conditionalConfigIndex = configs.findIndex(({ conditions }) => !conditions || !verifyConditions({ request, timestamp, conditions }));
        }
        if(conditionalConfigIndex === -1){
            response.body = {
                unmetCondition: 'any'
            };
            response.code(400);
            return response;
        }
        const _config = {
            ...config,
            ...typeof randomConfigIndex === 'number' ? configs[randomConfigIndex] : {},
            ...typeof conditionalConfigIndex === 'number' ? configs[conditionalConfigIndex] : {}
        };
        ({ url } = _config);
        const {
            params: configParams,
            iframe
        } = _config;
        if(configParams)
            url = stringSpliceMulti(url, configParams.map(({ index, key }) => ({ start: index, item: params[key] })));
        const _url = new URL(url);
        for(const [key, value] of Object.entries(params)){
            if(!configParams.some(param => param.key === key))
                _url.searchParams.append(key, value);
        }
        if(iframe){
            response.body = `
                <!DOCTYPE html>
                <html>
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>${iframe.title}</title>
                        <link rel="icon" href="${iframe.icon}" />
                        ${iframe.isPwa ? `
                            <link rel="manifest" href="data:application/manifest+json,{ &quot;name&quot;: &quot;${iframe.title}&quot;, &quot;short_name&quot;: &quot;${iframe.title}&quot; }">
                            <meta name="mobile-web-app-capable" content="yes">
                            <meta name="apple-mobile-web-app-capable" content="yes">
                        ` : ''}
                    </head>
                    <body>
                        <iframe
                            src="${_url}"
                            style="position: absolute; top: 0; left: 0; width: 100vw; height: 100vh; border: 0"
                        ></iframe>
                    </body>
                </html>
            `;
        }
        else {
            response.body = {
                url: _url,
                randomConfigIndex,
                conditionalConfigIndex
            };
            response.redirect(_url);
        }
        if(typeof response.body === 'object') response.body = {
            ...response.body,
            creationTimestamp: Number(creationTimestamp),
            config
        };
        if(webhookSecretConfig && webhook && request.headers.get('dnt') !== '1'){
            const t1 = Date.now();
            let status;
            try {
                ({ status } = await axios.post(
                    webhook.url,
                    {
                        id: path.id,
                        timestamp,
                        userAgent: webhook.body?.includes('userAgent') ? request.headers.get('user-agent') : undefined,
                        ip: webhook.body?.includes('ip') ? request.headers.get('x-forwarded-for') : undefined,
                        locale: webhook.body?.includes('locale') ? request.headers.get('accept-language') : undefined
                    },
                    {
                        timeout: webhookTimeout,
                        [`${webhookSecretConfig.webhookSecretType}s`]: {
                            [webhookSecretConfig.webhookSecretKey]: webhookSecretConfig.webhookSecretValue
                        }
                    }
                ));
            }
            catch(error){
                ({ status } = error.response || {});
                if(error?.message === 'The signal has been aborted'){
                    if(typeof response.body === 'object')
                        response.body.webhookResponseTimeMs = -1;
                }
                else if(!status)
                    console.error(error);
            }
            const t2 = Date.now();
            if(typeof response.body === 'object'){
                if(status)
                    response.body.webhookResponseTimeMs = t2 - t1;
                response.body.webhookResponseStatus = status;
            }
        }
        return response;
    };
    createRoute({
        method: 'GET',
        path: '/{id}',
        handler
    });
    createRoute({
        method: 'GET',
        path: '/{id}/t',
        handler
    });
}

createRoute({
    method: 'GET',
    path: '/{id}/info',
    handler: async (
        request,
        h,
        {
            password,
            response,
            path
        }
    ) => {
        const {
            rows: [[
                creationTimestamp,
                editionTimestamp,
                config,
                encryptedConfig,
                deletionTimestamp,
                deletionMessage
             ] = []]
        } = await db.queryArray(`
            SELECT
                "creationTimestamp",
                "editionTimestamp",
                "config",
                "encryptedConfig" ${password ? '' : 'IS NOT null'},
                "deletionTimestamp",
                "deletionMessage"
            FROM "url"
            WHERE "id" = $1
        `, [path.id]);
        let decryptedConfig;
        if(password){
            try {
                decryptedConfig = JSON.parse(await new Cryptr(password).decrypt(encryptedConfig));
            }
            catch {}
        }
        if(creationTimestamp) response.body = {
            creationTimestamp: Number(creationTimestamp),
            editionTimestamp: Number(editionTimestamp),
            config: decryptedConfig || config,
            isEncrypted: !!encryptedConfig,
            deletionTimestamp: deletionTimestamp && Number(deletionTimestamp),
            deletionMessage
        };
        else response.code(404);
        return response;
    }
});

createRoute({
    method: 'DELETE',
    path: '/{id}',
    schema: deleteSchema,
    handler: async (
        request,
        h,
        {
            password,
            path,
            body: {
                message
            } = {},
            response
        }
    ) => {
        const { rows: [[ exists, isDeleted, deletionCode ] = []] } = await db.queryArray(
            'SELECT true as "exists", "deletionTimestamp" IS NOT null AS "isDeleted", "deletionCode" FROM "url" WHERE "id" = $1',
            [path.id]
        );
        if(!exists){
            response.code(404);
            return response;
        }
        if(isDeleted){
            response.code(410);
            return response;
        }
        if(!deletionCode){
            response.code(405);
            return response;
        }
        if(!verify(password, deletionCode)){
            response.code(401);
            response.header('www-authenticate', 'Basic');
            return response;
        }
        const deletionTimestamp = Date.now();
        await db.queryArray(`
            UPDATE "url"
            SET
                "config" = null,
                "encryptedConfig" = null,
                "deletionCode" = null,
                "deletionTimestamp" = $deletionTimestamp
                ${message ? ', "deletionMessage" = $message' : ''}
            WHERE "id" = $id
        `, {
            deletionTimestamp,
            id: path.id,
            ...message ? { message } : {}
        });
        response.body = {
            deletionTimestamp
        };
        return response;
    }
});

createRoute({
    method: 'GET',
    path: '/{id}/vanity',
    schema: getVanitySchema,
    handler: async (
        request,
        h,
        {
            password,
            response,
            path
        }
    ) => {
        if(password){
            const {
                codeExists,
                urlExists,
                urlEditionCode
            } = await getVanity({
                code: password,
                id: path.id
            });
            if(codeExists){
                if(urlExists)
                    response.code(409);
            }
            else {
                if(urlExists){
                    if(verify(password, urlEditionCode))
                        response.body = { willEdit: true };
                    else {
                        response.code(401);
                        response.header('www-authenticate', 'Basic');
                    }
                }
                else {
                    response.code(401);
                    response.header('www-authenticate', 'Basic');
                }
            }
        }
        else {
            response.code(401);
            response.header('www-authenticate', 'Basic');
        }
        return response;
    }
});

server.start();