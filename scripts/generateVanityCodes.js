import db from '../lib/db.js';
import nanoid from '../lib/nanoid.js';

const codes = Array.from({ length: parseInt(Deno.args[0]) }, () => nanoid({ size: 30 }));

await db.queryArray(`
    INSERT INTO "vanityCode"
    VALUES ${codes.map((_, index) => `($${index+2}, $1)`).join(',')}
`, [Deno.args[1], ...codes]);

console.log(codes.join('\n'));