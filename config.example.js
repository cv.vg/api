export default {
    /**
     * Sentry DSN
     * @type {string}
     */
    sentryDsn: undefined,
    /**
     * Server listening port
     * @type {number}
     */
    port: undefined,
    /**
     * Website URL
     * @type {string}
     */
    webUrl: undefined,
    /**
     * Database username
     * @type {string}
     */
    dbUsername: undefined,
    /**
     * Database password
     * @type {string}
     */
    dbPassword: undefined,
    /**
     * Database name
     * @type {string}
     */
    dbName: undefined,
    /**
     * @type {string}
     */
    dbHost: undefined,
    /**
     * Database port
     * @type {number}
     */
    dbPort: undefined,
    /**
     * Webhook calls timeout
     * in milliseconds
     * @type {number}
     */
    webhookTimeout: undefined
};