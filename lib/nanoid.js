import { customAlphabet } from 'https://deno.land/x/nanoid@v3.0.0/mod.ts';
import alphanumeric from 'npm:nanoid-dictionary@4.3.0/alphanumeric.js';
export default ({ size }) => customAlphabet(alphanumeric, size)();