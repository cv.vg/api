export default db => ({
    getSetting: async key => JSON.parse((await db.queryArray('SELECT "value" FROM "setting" WHERE "key" = $1', [key])).rows[0][0]),
    setSetting: async object => await db.queryArray('UPDATE "setting" SET "value" = $2 WHERE "key" = $1', Object.entries(object)[0])
});