import { Client } from 'https://deno.land/x/postgres@v0.17.0/mod.ts';

import config from '../config.js';

const {
    dbUsername,
    dbPassword,
    dbName,
    dbHost,
    dbPort
} = config;

export default new Client({
    user: dbUsername,
    password: dbPassword,
    database: dbName,
    hostname: dbHost,
    port: dbPort
});