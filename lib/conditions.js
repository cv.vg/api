import { parseCronExpression } from 'npm:cron-schedule@3.0.6';
import dayjs from 'npm:dayjs@1.11.3';

const regexpFromString = string => {
    const [, pattern, flags] = string.split('/');
    return new RegExp(pattern, flags);
};

export default ({
    request,
    timestamp,
    conditions: {
        userAgent,
        ip,
        locale,
        expiration,
        cron
    }
}) => {
    if(userAgent && !regexpFromString(userAgent).test(request.headers.get('user-agent')))
        return 'userAgent';
    if(ip && !regexpFromString(ip).test(request.headers.get('x-forwarded-for')))
        return 'ip';
    if(locale && !regexpFromString(locale).test(request.headers.get('accept-language')))
        return 'locale';
    if(expiration && timestamp > expiration)
        return 'expiration';
    if(cron && !parseCronExpression(cron).matchDate(dayjs().startOf('minute').toDate()))
        return 'cron';
};