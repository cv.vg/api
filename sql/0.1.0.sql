CREATE TABLE "url" (
    "id"                  VARCHAR(10) PRIMARY KEY,
    "creationTimestamp"   BIGINT      NOT NULL,
    "config"              JSON,
    "encryptedConfig"     TEXT,
    "editionCode"         VARCHAR(60),
    "editionTimestamp"    BIGINT,
    "deletionCode"        VARCHAR(60),
    "deletionTimestamp"   BIGINT,
    "deletionMessage"     VARCHAR(255),
    "webhookSecretConfig" JSON
);

CREATE TABLE "setting" (
    "key"   VARCHAR(10) PRIMARY KEY,
    "value" VARCHAR(10) NOT NULL
);

INSERT INTO "setting" ("key", "value") VALUES ('idSize', 3);

CREATE TABLE "vanityCode" (
    "code"  VARCHAR(30) PRIMARY KEY,
    "label" VARCHAR(20)
);